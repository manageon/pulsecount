#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

#define DEVICE_FILENAME "/dev/eintdev"

int main()
{
	int dev;
	unsigned long wBuff, rBuff;
	int loop;

	dev  = open(DEVICE_FILENAME, O_RDWR|O_NDELAY);

	if(dev >= 0)
	{
		//buff = 0x123456FF;
		wBuff = 0;
		printf("write 0x%x\n", wBuff);
		write(dev, &wBuff, sizeof(wBuff));
		sleep(1);

		for(loop =0; loop < 100; loop++)
		{
			rBuff = 0;
			if(read(dev, &rBuff, sizeof(rBuff)) == sizeof(rBuff))
			{
				printf("read data [0x%X]\n", rBuff);
				sleep(1);
			}
			//wBuff++;
			//printf("write 0x%x\n", wBuff);
			//write(dev, &wBuff, sizeof(wBuff));
			//sleep(1);
		}
		close(dev);
	}

	return 0;
}
