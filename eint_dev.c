#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/fcntl.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/seqlock.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <asm/irq.h>
#include <asm/uaccess.h>
//#include <asm/io.h>

#define EINT_DEV_NAME		"eintdev"
#define EINT_DEV_MAJOR		240
#define EINT_WP_NUMBER		2

typedef struct
{
	unsigned long count;
	unsigned long dummy;
} __attribute__ ((packed)) R_INT_INFO;

seqlock_t the_lock = SEQLOCK_UNLOCKED;
unsigned int seq;

irqreturn_t pulse_interrupt(int irq, void *dev_id)
//irqreturn_t pulse_interrupt(int irq, void *dev_id, struct pt_regs *regs)
{
	R_INT_INFO *ptrInfo;

	write_seqlock(&the_lock);
	ptrInfo = (R_INT_INFO *) dev_id;
	ptrInfo->count++;
	write_sequnlock(&the_lock);
//	printk(KERN_INFO "[eint_dev][EINT] pulse count %ld\n", ptrInfo->count);
	
	return IRQ_HANDLED;
}

int eint_open(struct inode *inode, struct file *filp)
{
	R_INT_INFO *ptrInfo;
//	unsigned long flags;

	ptrInfo = kmalloc(sizeof(R_INT_INFO), GFP_KERNEL);
	filp->private_data = ptrInfo;
	ptrInfo->count = 0;
	ptrInfo->dummy = 1000;

	if(!request_irq(S3C_EINT(EINT_WP_NUMBER), pulse_interrupt, IRQF_DISABLED, EINT_DEV_NAME, ptrInfo))
	{
		set_irq_type(S3C_EINT(EINT_WP_NUMBER), IRQ_TYPE_EDGE_FALLING);
	}
	printk(KERN_INFO "[eint_dev] minor: %d\n", MINOR(inode->i_rdev));
	return 0;
}

ssize_t eint_read(struct file *filp, char *buf, size_t count, loff_t *f_pos)
{
	unsigned long status;
	R_INT_INFO *ptrInfo = (R_INT_INFO *)filp->private_data;
	
	do 
	{
		seq = read_seqbegin(&the_lock);
		status = ptrInfo->count;
		put_user(status, (unsigned long *)buf);
	} while (read_seqretry(&the_lock, seq));

	return count;
}

ssize_t eint_write(struct file *filp, const char *buf, size_t count, loff_t *f_pos)
{
	unsigned long status;
	R_INT_INFO *ptrInfo = (R_INT_INFO *)filp->private_data;

	write_seqlock(&the_lock);
	get_user(status, (unsigned long *)buf);
	ptrInfo->count = status;
	write_sequnlock(&the_lock);

	return count;
}

int eint_release(struct inode *inode, struct file *filp)
{
	R_INT_INFO *ptrInfo = (R_INT_INFO *)filp->private_data;
	
	disable_irq(IRQ_EINT(EINT_WP_NUMBER));
	free_irq(IRQ_EINT(EINT_WP_NUMBER), ptrInfo);
	kfree(ptrInfo);

	return 0;
}

struct file_operations eint_fops = 
{
	.owner   = THIS_MODULE,
	.read    = eint_read,
	.write   = eint_write,
	.open    = eint_open,
	.release = eint_release,
};

int eint_init(void)
{
	int result;
	result = register_chrdev(EINT_DEV_MAJOR, EINT_DEV_NAME, &eint_fops);
	if(result <0)
		return result;
	printk(KERN_INFO "[eint_dev] initialized\n");
	return 0;
}

void eint_exit(void)
{
	unregister_chrdev(EINT_DEV_MAJOR, EINT_DEV_NAME);
	printk(KERN_INFO "[eint_dev] exited\n");
}

module_init(eint_init);
module_exit(eint_exit);

MODULE_LICENSE("Dual BSD/GPL");
